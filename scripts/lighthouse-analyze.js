import path from "node:path";
import fs from "node:fs";

const pathname = path.resolve(path.dirname("."), "lighthouse.json");
const data = JSON.parse(fs.readFileSync(pathname, "utf8"));

const failed_audit = [];

for (const [key, value] of Object.entries(data.audits)) {
  if (isFailedScore(value.score)) {
    failed_audit.push({
      name: key,
      score: value.score,
      description: value.description,
    });
  }
}

for (const [key, value] of Object.entries(data.categories)) {
  if (isFailedScore(value.score)) {
    failed_audit.push({
      name: key,
      score: value.score,
      description: value.description || value.title,
    });
  }
}

if (failed_audit.length > 0) {
  fs.writeFileSync(
    path.resolve(path.dirname("."), "lighthouse-failed.json"),
    JSON.stringify(failed_audit, null, 2)
  );
  console.log("Failed audits:");
  console.table(
    failed_audit.map((x) => ({
      ...x,
      description: x.description.slice(0, 50),
    }))
  );
  process.exit(1);
}

function isFailedScore(score) {
  return score !== null && score > 0 && score < 1;
}
